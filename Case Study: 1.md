Case Study: 1

Interns are required to create a signup/login page : 

Signup: User should be able to signup using following methods:
A. Create Own user id and password (this should be validated against existing user id and passwords in database). In case same user id/password doesn’t exist then user shall be able to create another one.

Sign-In : 
a. User Id and Password: User should be able to login with his/her credentials
b. Social Media Login I.e. Facebook/linkedin - On these login they shall be able to link their existing user id account the first time.
c. Post Login reach to a blank dashboard with only one option for “Add Vendor”.
d. In “Add Vendor” they shall be able to take Vendor’s data I.e. their Company name, logo, address, GST number, list of services offered, contact person details details (name/phone/email/linkedin id/image) as input. 

Validate these fields e.g. 
a. image can be either jpg/png/svg, should be cropped at frontend and cannot be more than (say 50kb) in size.
b. Email should be validated by sending an email OTP and verifying it
c. Mobile number to be verified by sending an sms OTP and verifying it

Once the form is submitted data should be stored in a database and visible as list of vendors.

Case Study 2: 
Intern should be able to create a YouTube like page as a web interface to show all vendors 
1. Top Menu showing some options
2. Left Menu showing search criteria based on Vendor name and services offered
3. Right side showing thumbnails showing Vendor lists, their logo and comma separated services, along with phone number




Starting from base:

1. Backend:
    - Postgres
        - Table: 
            - users(id, password)
            - vendors(id, name, logo, address, GST number, list of services offered, contacts)
            - contacts(name, phone, email, linkedin id, image)
    
    - Sequelize - for ORM
    - API:
        - /
        - /user-register
        - /user-login
        - /user-edit
        - /user-delete
        - /vendor-add
        - /vendor-edit
        - /vendor-delete


2. Frontend: 
    - React
        - Pages:
            - Landing  - /
            - Register - /register
            - Sign in - /signin
            - Profile - /profile
                - forget passoword
            - Vendors - /home



    Prisma
    Vite


Register: Email for recovery
Axios for email, phone verification 
