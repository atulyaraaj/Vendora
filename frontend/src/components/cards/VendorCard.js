import '../css/cards/VendorCard.css';
import { FaStore } from 'react-icons/fa';
import { IoMailSharp } from "react-icons/io5";
import { FaPhoneFlip } from "react-icons/fa6";
import { FcBusiness } from "react-icons/fc";
import { FaMapLocationDot } from "react-icons/fa6";

export default function VendorCard(props) {
    return <div className="card">

        <div className='fields'>
            <div className='icon'><FaStore size={30} /></div>
            <div><h2>{props.vendor.name}</h2>
            </div>
        </div>
        <div className='fields'>
            <div><IoMailSharp size={20} /></div>
            <div><h3>{props.vendor.email}</h3>
            </div>
        </div>
        <div className='fields'>
            <div><FaPhoneFlip size={20} /></div>
            <div><h3>{props.vendor.phone}</h3>
            </div>
        </div>

         <div className='fields'>
            <div><FcBusiness size={20} /></div>
            <div><h3>{props.vendor.gst_number}</h3></div>
        </div>
        <div className='fields'>
            <div><FaMapLocationDot size={20} /></div>
            <div><h3>{props.vendor.address}</h3></div>
        </div> 
    </div >;

}