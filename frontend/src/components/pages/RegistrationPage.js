import React, { Component } from "react";
import { Link } from 'react-router-dom'
import AuthService from "../../services/auth.service";



import '../css/RegistrationPage.css'
import '../css/LandingPage.css'
import BackgroundImage from '../../assets/images/bg.jpg'
import Footer from "./Footer";


export default class RegistrationPage extends Component {


    constructor(props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeRePassword = this.onChangeRePassword.bind(this);

        this.state = {
            username: "",
            password: "",
            re_password: "",
            successful: false,
            message: ""
        };
    }

    onChangeUsername(e) {
        this.setState({
            username: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    onChangeRePassword(e) {
        this.setState({
            re_password: e.target.value
        });
    }




    handleRegister(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });
        if (this.state.password === this.state.re_password) {
            AuthService.register(
                this.state.username,
                this.state.password
            ).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        } else {
            this.setState({
                successful: false,
                message: "Password does not Match!"
            })
        }
    }


    render() {
        return (
            <div>
                <header style={HeaderStyle}>
                <div className="text-center m-5-auto base">
                    <div id='form-content'>
                        <h2>Join Us</h2>
                        <h5>Create your personal account</h5>
                        {!this.state.successful && (
                            <form onSubmit={this.handleRegister}>
                                <p>
                                <label>Username</label><br />
                                    <input type="text"
                                        value={this.state.username}
                                        onChange={this.onChangeUsername}
                                        required />
                                </p>
                                <p>
                                    <label>Password</label><br />
                                    <input type="password"
                                        value={this.state.password}
                                        onChange={this.onChangePassword}
                                        required />
                                </p>

                                <p>
                                    <label>Confirm Password</label><br />
                                    <input type="password"
                                        value={this.state.re_password}
                                        onChange={this.onChangeRePassword}
                                        required />
                                </p>
                                <p>
                                    <button className="primary-button" type="submit">Register</button>
                                </p>
                            </form>)}

                        <footer>
                            <Link id='back' to="/">Back</Link>
                        </footer>

                        {this.state.message && (
                            <div className="base-message text-center">
                                <h4>{this.state.message}</h4>
                            </div>
                        )}

                    </div>

                </div>
            </header>
            <Footer/>
            </div>
        )

    }
}

const HeaderStyle = {
    width: "100%",
    height: "100vh",
    background: `url(${BackgroundImage})`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
}
