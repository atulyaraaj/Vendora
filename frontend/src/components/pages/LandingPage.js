import React from 'react'
import { Link } from 'react-router-dom'

import '../css/LandingPage.css'
import BackgroundImage from '../../assets/images/bg.jpg'
import Footer from './Footer'

export default function LandingPage() {
    return (
        <div>
            <header style={HeaderStyle}>
                <div className=' base'>
                    <div className="main-title text-center">Welcome to Vendora</div>
                    <div className="buttons text-center">
                        <Link to="/signin">
                            <button className="primary-button">log in</button>
                        </Link>
                        <Link to="/register">
                            <button className="primary-button" id="reg_btn"><span>register </span></button>
                        </Link>
                    </div>
                </div>
            </header>
            <Footer />
        </div>
    )
}

const HeaderStyle = {
    width: "100%",
    height: "100vh",
    background: `url(${BackgroundImage})`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
}