import React, { Component } from "react";
import { Link } from "react-router-dom";
import AuthService from "../../services/auth.service";
import { withRouter } from "../../common/withRouter";

import "../css/RegistrationPage.css";
import "../css/LandingPage.css";
import BackgroundImage from "../../assets/images/bg.jpg";
import Footer from "./Footer";

class SignInPage extends Component {
  constructor(props) {
    super(props);
    this.handleSignIn = this.handleSignIn.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);

    this.state = {
      username: "",
      password: "",
      successful: false,
      message: "",
    };
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value,
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value,
    });
  }

  async handleSignIn(e) {
    e.preventDefault();

    this.setState({
      message: "",
      successful: false,
    });
    await AuthService.login(this.state.username, this.state.password).then(
      () => {
        this.props.router.navigate("/home");
        window.location.reload();
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          successful: false,
          message: resMessage,
        });
      }
    );
  }

  render() {
    return (
      <div>
        <header style={HeaderStyle}>
          <div className="text-center m-5-auto base">
            <div id="form-content">
              <h2>Welcome Back!</h2>
              {!this.state.successful && (
                <form onSubmit={this.handleSignIn}>
                  <p>
                    <label>Username</label>
                    <br />
                    <input
                      type="text"
                      value={this.state.username}
                      onChange={this.onChangeUsername}
                      required
                    />
                  </p>
                  <p>
                    <label>Password</label>
                    <br />
                    <input
                      type="password"
                      value={this.state.password}
                      onChange={this.onChangePassword}
                      required
                    />
                  </p>
                  <p>
                    <button className="primary-button" type="submit">
                      Login
                    </button>
                  </p>
                </form>
              )}

              <footer>
                <Link id="back" to="/">
                  Back
                </Link>
              </footer>

              {this.state.message && (
                <div className="base-message text-center">
                  <h4>{this.state.message}</h4>
                </div>
              )}
            </div>
          </div>
        </header>
        <Footer />
      </div>
    );
  }
}

const HeaderStyle = {
  width: "100%",
  height: "100vh",
  background: `url(${BackgroundImage})`,
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
};

export default withRouter(SignInPage);
