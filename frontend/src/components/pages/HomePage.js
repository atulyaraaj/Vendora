import React, { Component } from "react";
import { Navigate } from "react-router-dom";
import AuthService from "../../services/auth.service";
import '../css/HomePage.css';
import '../css/RegistrationPage.css';
import logo from '../../assets/images/logo.png';
import ProfileModal from '../overlays/Profile';
import VendorFormModal from '../overlays/VendorForm';
import VendorCard from "../cards/VendorCard";
import VendorService from "../../services/VendorService";



export default class HomePage extends Component {
    constructor(props) {
        super(props);
        this.setIsOpen = this.setIsOpen.bind(this);
        this.formIsOpen = this.formIsOpen.bind(this);
        this.getAllVendors = this.getAllVendors.bind(this);
        this.state = {
            formOpen: false,
            isOpen: false,
            redirect: null,
            userReady: false,
            currentUser: { username: "" },
            vendorsData: null
        };
    }

    setIsOpen(e) {
        this.setState({
            isOpen: e
        });
    }

    formIsOpen(e) {
        this.setState({
            formOpen: e
        });

    }

    getAllVendors(e) {
        const vendorsRespond = VendorService.allVendors();
        // this.setState({vendorsData: [vendorsRespond]})
        // console.log(vendorsRespond);

        const data = vendorsRespond.then((result) => {
            console.log(result);
            if (result) {
                console.log(result.data.vendors);

                this.setState({ vendorsData: result.data.vendors });
                return;

            }
            this.setState({ vendorsData: [] })
        })
    }

    componentDidMount() {
        const currentUser = AuthService.getCurrentUser();
        if (!currentUser) this.setState({ redirect: "/" });
        this.setState({ currentUser: currentUser, userReady: true })
        document.body.style.backgroundColor = "#bbffcd";
        this.getAllVendors();
    }





    render() {
        if (this.state.redirect) {
            return <Navigate to={this.state.redirect} />
        }
        const { currentUser } = this.state;
        return (
            <div>
                {(this.state.userReady) ?
                    <div>
                        <header id="header">
                            <nav className="nav-bar flex">
                                <img src={logo} />
                                <div>
                                    <button className="primaryBtn" onClick={() => this.setIsOpen(true)}>
                                        Profile
                                    </button>
                                </div>
                                <div>
                                    <button className="primaryBtn"
                                        onClick={() => this.formIsOpen(true)}
                                    >
                                        Add Vendor
                                    </button>
                                </div>

                            </nav>
                        </header>

                        <body id="main-body">

                            <div id="side-nav">

                                <form>

                                    <p>
                                        <input type="text"
                                            placeholder="Search Vendor"
                                            onChange={this.onChangeUsername}
                                            required />
                                    </p>


                                </form>


                            </div>

                            <div id="main-content">

                                {
                                    this.state.vendorsData ?

                                        this.state.vendorsData.map(vendor =>

                                            <VendorCard vendor={vendor} />

                                        )

                                        :

                                        <div id="no-vendor">
                                            No Vendors found.
                                        </div>
                                }
                            </div>
                        </body>
                    </div> : null}
                {this.state.isOpen && <ProfileModal setIsOpen={this.setIsOpen} user={currentUser.username}
                />}
                {this.state.formOpen && <VendorFormModal formIsOpen={this.formIsOpen} getAllVendors={this.getAllVendors} userId={currentUser.id} />}
            </div>
        );
    }
}
