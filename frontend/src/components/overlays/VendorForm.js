import React, { useState } from "react";
import '../css/overlays/Vendor.css';
import { RiCloseLine } from "react-icons/ri";
import VendorService from "../../services/VendorService";

export default function VendorFormModal({
	formIsOpen,
	userId,
	getAllVendors
}) {

	const [v_name, onNameChange] = useState("");
	const [v_gst, onGstChange] = useState("");
	const [v_address, onAddressChange] = useState("");
	const [v_email, onEmailChange] = useState("");
	const [v_phone, onPhoneChange] = useState("");


	const onNameChangeHandler = event => {
		onNameChange(event.target.value)
	}

	const onGstChangeHandler = event => {
		onGstChange(event.target.value)
	}

	const onAddressChangeHandler = event => {
		onAddressChange(event.target.value)
	}

	const onEmailChangeHandler = event => {
		onEmailChange(event.target.value)
	}

	const onPhoneChangeHandler = event => {
		onPhoneChange(event.target.value)
	}

	function handleAddVendor() {
		{
			const vendor = {
				name: v_name,
				email: v_email,
				gst_number: v_gst,
				address: v_address,
				userId: userId,
				phone: v_phone
			}
			VendorService.addVendor(vendor);
			getAllVendors();
			onNameChange("");
			onAddressChange("");
			onGstChange("");
			onEmailChange("");
			onPhoneChange("");
		}
	}




	return <>
		<div className="vendor-darkBG" onClick={() => formIsOpen(false)} />

		<div className="vendor-centered">
			<div className="vendor-modal">
				<div className="vendor-modalHeader">
					<h5 className="vendor-heading">Vendor Form</h5>

					<button className="vendor-closeBtn" onClick={() => formIsOpen(false)}>
						<RiCloseLine style={{ marginBottom: "-3px" }} />
					</button>

					<div className="vendor-modalActions">
						<div className="vendor-actionsContainer">
							<button
								className="vendor-cancelBtn"
								onClick={() => {

									formIsOpen(false);
								}}
							>
								Cancel
							</button>
						</div>
					</div>
				</div>

				<div className="vendor-modalContent">

					<form onSubmit={handleAddVendor} >

						<p>
							<label>Vendor Name</label><br />

							<input type="text"
								value={v_name}
								onChange={onNameChangeHandler}
								required />
						</p>


						<p>
							<label>Vendor Email</label><br />

							<input type="email"
								value={v_email}
								onChange={onEmailChangeHandler}
								required />
						</p>

						<p>
							<label>Vendor Phone</label><br />

							<input type="number" maxLength={10}
								value={v_phone}
								onChange={onPhoneChangeHandler} />
						</p>

						<p>
							<label>GST Number</label><br />

							<input type="text"
								value={v_gst}
								onChange={onGstChangeHandler}
								required />
						</p>

						<p>
							<label>Address</label><br />
							<input type="text"
								value={v_address}
								onChange={onAddressChangeHandler}
								required />
						</p>

						<button type="submit" className="vendor-deleteBtn">
							Add
						</button>
					</form>



				</div>

			</div>



		</div>
	</>;
};
