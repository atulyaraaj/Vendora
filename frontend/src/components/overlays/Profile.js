import React from "react";
import '../css/overlays/Profile.css';
import { RiCloseLine } from "react-icons/ri";
import AuthService from "../../services/auth.service";


export default function ProfileModal({ setIsOpen, user, redirector }) {


	const onLogoutClick = () => { // use consts or let
		
		AuthService.logout();

	}

	


	return <>
		<div className="darkBG" onClick={() => setIsOpen(false)} />

		<div className="centered">
			<div className="modal">
				<div className="modalHeader">
					<h5 className="heading">Howdy! {user}</h5>

					<button className="closeBtn" onClick={() => setIsOpen(false)}>
						<RiCloseLine style={{ marginBottom: "-3px" }} />
					</button>

					<div className="modalContent">
						Are you sure you want to delete the item?
					</div>

					<div className="modalActions">
						<div className="actionsContainer">
							<button className="deleteBtn" onClick={() => {

								onLogoutClick();
								setIsOpen(false);
								window.location.reload(false);

								
								
							}}>
								Log out
							</button>
							<button
								className="cancelBtn"
								onClick={() => {

									setIsOpen(false);
								}}
							>
								Cancel
							</button>
						</div>
					</div>

				</div>
			</div>
		</div>
	</>;
};
