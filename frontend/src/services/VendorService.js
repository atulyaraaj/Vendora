import axios from "axios";

const API_URL = "http://localhost:8080/api/vendor/";

class VendorService {

    async addVendor(vendorData) {

        axios.post(API_URL + "add", vendorData).
            then(res => {
                console.log(res);
            })

    }

    async allVendors() {
        return await axios.get(API_URL + "allVendors").then((result)=>{
            // console.log(result.data.vendors);
            return result;
        }).catch(err => {
            console.log(err);
        })
    }
}

export default new VendorService();
