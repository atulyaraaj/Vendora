import React from 'react'
import { Routes, Route } from 'react-router-dom'
import './App.css'


import LandingPage from './components/pages/LandingPage';
import RegistrationPage from './components/pages/RegistrationPage';
import Footer from './components/pages/Footer';
import SignInPage from './components/pages/SignInPage';
import HomePage from './components/pages/HomePage'


function App() {
  return (

    <div>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/signin" Component={SignInPage} />
        <Route path="/register" Component={RegistrationPage} />
        {/* {/* <Route path="/profile" component={ForgetPasswordPage} /> */}
          <Route path="/home" element={<HomePage/>} />
      </Routes>
    </div>
  );
}

export default App;
