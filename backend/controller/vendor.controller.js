const db = require("../models");
const Vendors = db.vendors;

exports.createVendor = (req, res) => {

    const vendor = {
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        logo: req.body.logo,
        gst_number: req.body.gst_number,
        address: req.body.address,
        userId: req.body.userId
    }
    Vendors.create(vendor).then(
        // res.status(201).send({ message: "Vendor was addded successfully!" 
        (resFromDb) => {

            res.send({ message: resFromDb });

            // console.log("$$$$$$$$$$$$$$$$$$");
            // console.log(resFromDb);
            // console.log("$$$$$$$$$$$$$$$$$$");
        }

    )
        .catch((err) => {
            console.log(">> Error while creating vendor: ", err);
        });

};

exports.findVendorByName = (req, res) => {
    Vendors.findAll(
        { where: { name: req.body.name } }
    ).then(vendor => {

        res.status(200).send({
            name: vendor.name,
            logo: vendor.logo,
            gst_number: vendor.gst_number,
            address: vendor.address
        })

    }
    ).catch(err, () => {
        console.log("Error while finding vendor: ", err)
    })

}


exports.findAllVendors = (req, res) => {
    Vendors.findAll().then(vendors => {
        // console.log(vendors);
        res.status(200).send({vendors});
    });
}

