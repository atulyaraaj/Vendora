const authJwt = require("./authJWT");
const verifyRegistration = require("./verifyRegistration");

module.exports = {
  authJwt,
  verifyRegistration
};
