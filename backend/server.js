const express = require("express");
const cors = require("cors");
require("dotenv").config();

// 6. Connecting all files to single file 'this'.


const app = express();

var corsOptions = {
    origin: "http://localhost:3000"
}

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));


const db = require("./models");

db.sequelize.sync()
  .then(() => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

// db.sequelize.sync({force: true}).then(() => {
//   console.log('Drop and Resync Db');
//   // initial();
// });


app.get('/', (rq, rs) => {
    rs.json({
        message: "Welcome to Atulya Backend!"
    });
});


// Adding inner routes to app object

// require('./routes/user.route')(app);
require('./routes/auth.routes')(app);
require('./routes/vendor.routes')(app);

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`Server is live on port ${PORT}`);
});


