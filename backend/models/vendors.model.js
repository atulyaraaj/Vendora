
module.exports = (sequelize, DataTypes) => {

    const Vendors = sequelize.define("vendors", {
        userId:{
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            foreignKey: true,
            onDelete: "cascade"
        },
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        phone: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        logo: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        gst_number: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        address:{
            type: DataTypes.STRING,
            allowNull: true
        }
    });

    return Vendors;
}