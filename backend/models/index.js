const dbConfig = require('../config/dbConfig');
// const Sequelize = require('sequelize');
const { DataTypes, Sequelize } = require("sequelize");


//2. Connect to database using Sequelize. 
// Sequelize is ORM Object Relational Mapping 


const sequelize = new Sequelize(
    dbConfig.DB,
    dbConfig.USER,
    dbConfig.PASSWORD,
    {
        host: dbConfig.HOST,
        port: dbConfig.port,
        dialect: dbConfig.dialect,
        pool: {
            max: dbConfig.pool.max,
            min: dbConfig.pool.min,
            acquire: dbConfig.pool.acquire,
            idle: dbConfig.pool.idle
        }
    }
)

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.dataTypes = DataTypes;

// Pass Sequelize and sequelize to users variable.

db.users = require("./users.model.js")(sequelize, db.dataTypes);
db.vendors = require("./vendors.model.js")(sequelize, db.dataTypes);

db.users.hasMany(db.vendors, { as: "vendors" });
db.vendors.belongsTo(db.users)

module.exports = db;

