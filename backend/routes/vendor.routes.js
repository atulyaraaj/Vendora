
const controller = require('../controller/vendor.controller');


module.exports = function (app){
    app.post("/api/vendor/add", controller.createVendor);
    app.post("/api/vendor/find", controller.findVendorByName);
    app.get("/api/vendor/allVendors", controller.findAllVendors);
}